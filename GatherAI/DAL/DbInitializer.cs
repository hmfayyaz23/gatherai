﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using GatherAI.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace GatherAI.DAL
{
    public class DbInitializer
    {
        private const string bucketName = "gather-ai-sync";
        private const string keyName = "DW/inventoryitems.csv";
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USEast2;
        private static IAmazonS3 client;
        public static void Initialize(GatherAiContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (!context.Products.Any())
            {
                var products = new Product[]
                {
                new Product{Name="Fayyaz"},
                new Product{Name="Tahir"},
                };
                foreach (Product s in products)
                {
                    context.Products.Add(s);
                }
                context.SaveChanges();
            }

            if (!context.InventoryItems.Any())
            {
                client = new AmazonS3Client(bucketRegion);
                ReadObjectDataAsync(context).Wait();
            }
        }
        public static async Task ReadObjectDataAsync(GatherAiContext context)
        {
            string responseBody = "";
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName
                };
                using (GetObjectResponse response = await client.GetObjectAsync(request))
                using (Stream responseStream = response.ResponseStream)
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    string title = response.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
                    string contentType = response.Headers["Content-Type"];
                    Console.WriteLine("Object metadata, Title: {0}", title);
                    Console.WriteLine("Content type: {0}", contentType);
                    responseBody = reader.ReadToEnd(); // Now you process the response body.

                    var lines = responseBody.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Skip(1);

                    foreach (var item in lines)
                    {
                        string[] values = item.Split(',');
                        InventoryItem model = new InventoryItem
                        {
                            ProductID = int.Parse(values[0]),
                            MissionBinID = int.Parse(values[1]),
                            Code = values[2],
                            Notes = values[3],
                            Quantity = int.Parse(values[4]),
                            DaysLeft = int.Parse(values[5]),
                            CreatedOn = values[6],
                            CreatedBy = int.Parse(values[7]),
                            ModifiedOn = values[8],
                            ModifiedBy = int.Parse(values[9]),
                            RowStatusID = int.Parse(values[10]),
                            RowVersion = int.Parse(values[11]),
                        };
                        context.InventoryItems.Add(model);
                    }
                    context.SaveChanges();
                }
            }

            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }
    }
}
