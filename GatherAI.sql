use GatherAI;

ALTER TABLE RowStatus
MODIFY COLUMN ModifiedBy int;

CREATE TABLE RowStatus
(
RowStatusID int NOT NULL auto_increment,
Name varchar(255),
CreatedOn DateTime,
CreatedBy int,    
ModifiedOn DateTime,
ModifiedBy int, 
RowStatus_ID int,
RowVersion int,

PRIMARY KEY (RowStatusID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatus_ID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Warehouse
(
WarehouseID int NOT NULL auto_increment,
Code varchar(255),
Name varchar(255),
Address varchar(255),
Latitude decimal,
Longitude decimal,
Levels int,
CreatedOn DateTime,
CreatedBy int,  
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (WarehouseID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE StructureType 
(
StructureTypeID int NOT NULL auto_increment,
Name varchar(255),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (StructureTypeID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE SideMaster 
(
SideID int NOT NULL auto_increment,
Name varchar(255),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (SideID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Structure 
(
StructureID int NOT NULL auto_increment,
WarehouseID int,
Level int,
Title varchar(255),
Code varchar(255),
Position int,
SideID int,
StructureTypeID int,
ParentStructureID int, 
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (StructureID),
FOREIGN KEY (WarehouseID) REFERENCES Warehouse(WarehouseID),
FOREIGN KEY (SideID) REFERENCES SideMaster(SideID),
FOREIGN KEY (StructureTypeID) REFERENCES StructureType(StructureTypeID),
FOREIGN KEY (ParentStructureID) REFERENCES Structure(StructureID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Bin 
(
BinID int NOT NULL auto_increment,
StructureID int,
Name varchar(255),
Code varchar(255),
Position int,
Width int,
Height int,
SideID int,
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (BinID),
FOREIGN KEY (StructureID) REFERENCES Structure(StructureID),
FOREIGN KEY (SideID) REFERENCES SideMaster(SideID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Product 
(
ProductID int NOT NULL auto_increment,
WarehouseID int,
Name varchar(255),
Code varchar(255),
SKU varchar(255),
UPC varchar(255),
Description varchar(255),
ItemsPerPallet int,
MonthlyUsage int,

CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (ProductID),
FOREIGN KEY (WarehouseID) REFERENCES Warehouse(WarehouseID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE InventoryItem 
(
InventoryItemID int NOT NULL auto_increment,
ProductID int,
MissionBinID int,
Code varchar(255),
Notes varchar(255),
Quantity int,
DaysLeft int,

CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (InventoryItemID),
FOREIGN KEY (ProductID) REFERENCES Product(ProductID),
FOREIGN KEY (MissionBinID) REFERENCES MissionBin(MissionBinID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE InventoryGallery 
(
InventoryGalleryID int NOT NULL auto_increment,
InventoryItemID int,
ImagePath varchar(255),
ThumbnailPath varchar(255),

CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (InventoryGalleryID),
FOREIGN KEY (InventoryItemID) REFERENCES InventoryItem(InventoryItemID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE MissionStatus  
(
MissionStatusID int NOT NULL auto_increment,
Name varchar(255),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (MissionStatusID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);


CREATE TABLE Mission
(
MissionID int NOT NULL auto_increment,
WarehouseID int,
Name varchar(255),
Date DateTime,
Directory varchar(255),
MissionStatusID int,
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (MissionID),
FOREIGN KEY (WarehouseID) REFERENCES Warehouse(WarehouseID),
FOREIGN KEY (MissionStatusID) REFERENCES MissionStatus(MissionStatusID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);


CREATE TABLE MissionBin
(
MissionBinID int NOT NULL auto_increment,
BinID int,
MissionID int,
IsSelected Boolean,
IsScanned Boolean,
IsChanged Boolean,
HasError Boolean,
Notes varchar(255),
ScannedOn DateTime,
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (MissionBinID),
FOREIGN KEY (BinID) REFERENCES Bin(BinID),
FOREIGN KEY (MissionID) REFERENCES Mission(MissionID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);


CREATE TABLE Team
(
TeamID int NOT NULL auto_increment,
Name varchar(255),
Address varchar(255),
BillingAddress varchar(255),
ContactName	varchar(255),
Email varchar(255),
Phone varchar(255),
Mobile varchar(255),
WarehouseID	int,
SubscriptionID int,
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (TeamID),
FOREIGN KEY (WarehouseID) REFERENCES Warehouse(WarehouseID),
FOREIGN KEY (SubscriptionID) REFERENCES Subscription(SubscriptionID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE MemberType
(
MemberTypeID int NOT NULL auto_increment,
Name varchar(255),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (MemberTypeID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

ALTER TABLE UserProfile
ADD CONSTRAINT FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID);

CREATE TABLE UserProfile
(
UserProfileID int NOT NULL auto_increment,
TeamID int,
FirstName varchar(255),
LastName varchar(255),
MemberTypeID int,
Email varchar(255),
Mobile varchar(255),
Password varchar(255),
CreatedOn DateTime,
CreatedBy int NULL,
ModifiedOn DateTime,
ModifiedBy int NULL,
RowStatusID int, 
RowVersion int,

PRIMARY KEY (UserProfileID),
FOREIGN KEY (TeamID) REFERENCES Team(TeamID),
FOREIGN KEY (MemberTypeID) REFERENCES MemberType(MemberTypeID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Invitation
(
InvitationID int NOT NULL auto_increment,
UserProfileID int,
Code varchar(255),
ExpiresOn DateTime,
UtilizedOn DateTime,
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (InvitationID),
FOREIGN KEY (UserProfileID) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Package
(
PackageID int NOT NULL auto_increment,
Title varchar(255),
Description	varchar(255),
MonthCount int,
WarehouseCount int,
Price decimal(15,2),
Discount decimal(15,2),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (PackageID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Subscription
(
SubscriptionID int NOT NULL auto_increment,
PackageID int,
LastPaymentDate	DateTime,
NextPaymentDate	DateTime,
WarehouseCount int,
MonthCount int,
Price decimal(15,2),
Discount decimal(15,2),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (SubscriptionID),
FOREIGN KEY (PackageID) REFERENCES Package(PackageID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Invoice
(
InvoiceID int NOT NULL auto_increment,
InvoiceNumber varchar(255),
SubscriptionID int,
InvoiceTo int,
invoiceDate	DateTime,
DueDate	DateTime,
Amount decimal(15,2),
Discount decimal(15,2),
GraceDays int,
Penalty	decimal(15,2),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (InvoiceID),
FOREIGN KEY (SubscriptionID) REFERENCES Subscription(SubscriptionID),
FOREIGN KEY (InvoiceTo) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE PaymentMethod 
(
PaymentMethodID int NOT NULL auto_increment,
Name varchar(255),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (PaymentMethodID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);

CREATE TABLE Receipt 
(
ReceiptID int NOT NULL auto_increment,
ReceiptNumber varchar(255),
InvoiceID int,
ReceivedBy int,
ReceiptDate	DateTime,
InvoiceAmount decimal(15,2),
LateFeeAmount decimal(15,2),
LateFeeWaiver decimal(15,2),
Amount decimal(15,2),
PaymentMethodID	int,
PaymentReference1 varchar(255),
PaymentReference2 varchar(255),
Remarks	varchar(255),
CreatedOn DateTime,
CreatedBy int, 
ModifiedOn DateTime,
ModifiedBy int, 
RowStatusID int, 
RowVersion int,

PRIMARY KEY (ReceiptID),
FOREIGN KEY (InvoiceID) REFERENCES Invoice(InvoiceID),
FOREIGN KEY (ReceivedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (PaymentMethodID) REFERENCES PaymentMethod(PaymentMethodID),
FOREIGN KEY (CreatedBy) REFERENCES UserProfile(UserProfileID), 
FOREIGN KEY (ModifiedBy) REFERENCES UserProfile(UserProfileID),
FOREIGN KEY (RowStatusID) REFERENCES RowStatus(RowStatusID)
);




        
