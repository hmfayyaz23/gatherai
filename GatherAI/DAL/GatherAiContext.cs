﻿using GatherAI.Models;
using Microsoft.EntityFrameworkCore;

namespace GatherAI.DAL
{
    public class GatherAiContext : DbContext
    {
        public GatherAiContext(DbContextOptions<GatherAiContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<InventoryItem>().ToTable("InventoryItem");
        }
    }
}