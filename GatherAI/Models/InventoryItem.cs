﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GatherAI.Models
{
    public class InventoryItem
    {
        public int InventoryItemID { get; set; }
        public int ProductID { get; set; }
        public int MissionBinID { get; set; }
        public string Code { get; set; }
        public string Notes { get; set; }
        public int Quantity { get; set; }
        public int DaysLeft { get; set; }
        public string CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int RowStatusID { get; set; }
        public int RowVersion { get; set; }
        public Product Product { get; set; }
    }
}