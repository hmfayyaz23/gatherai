﻿using System;
using System.Collections.Generic;

namespace GatherAI.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }

    }
}